package router

import (
	"gofar/packed/cfg"
	"html/template"
	"net/http"

	gintemplate "github.com/foolin/gin-template"
	"github.com/gin-gonic/gin"
)

// 模板引擎初始化
func templateInit(r *gin.Engine) {
	// 静态文件
	r.StaticFS(cfg.Get().Server.ServerRoot, http.Dir("public"))
	r.HTMLRender = gintemplate.New(gintemplate.TemplateConfig{
		Root:      "views/gofar-ui",
		Extension: ".html",
		Master:    "layouts/main",
		Delims:    gintemplate.Delims{Left: "${", Right: "}"},
		Funcs:     template.FuncMap{},
	})
}

// 注册路由
func Register(r *gin.Engine) {
	// 初始化模板配置
	templateInit(r)
	// 前台UI注册
	views(r)
	// v1 版api注册
	v1api(r)
}
