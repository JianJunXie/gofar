package router

import (
	"gofar/app/controller"

	"github.com/gin-gonic/gin"
)

func v1api(r *gin.Engine) {
	// API接口
	api := r.Group("/api/v1")
	{
		api.POST("/login", controller.User.Login)
		api.POST("/install", controller.System.Install)
	}
}
