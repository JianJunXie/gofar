package router

import (
	"gofar/app/controller"

	"github.com/gin-gonic/gin"
)

func views(r *gin.Engine) {
	// web router
	{
		// 首页
		r.GET("/", controller.View.Home)
		// 安装页面
		r.GET("/install", controller.View.Install)
	}
}
