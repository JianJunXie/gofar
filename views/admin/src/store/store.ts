import { createStore } from 'vuex'

const store = createStore({
  state: {
    // login
    token: localStorage.auth_token as string, // TOKEN
    refreshToken: localStorage.refresh_token as string, // 刷新token

    // user
    userInfo: {},
  },
  mutations: {},
})

export default store
