import { createRouter, createWebHashHistory } from 'vue-router'

import HelloWorld from 'src/components/HelloWorld.vue'

const routes = [{ path: '/hello', component: HelloWorld }]

export default createRouter({ history: createWebHashHistory(), routes })
