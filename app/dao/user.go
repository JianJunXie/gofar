package dao

import (
	"gofar/app/model"
	"gofar/packed/orm"

	"gorm.io/gorm"
)

var User = &userDao{}

type userDao struct{}

func (*userDao) Create(user *model.User) *gorm.DB {
	return orm.DB().Select("Account", "Name", "Passwd").Create(user)
}
