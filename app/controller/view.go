package controller

import (
	"gofar/packed/cfg"
	"gofar/packed/gerr"
	"gofar/packed/resp"
	"net/http"

	"github.com/gin-gonic/gin"
)

var View = &viewApi{}

type viewApi struct{}

func (*viewApi) Home(c *gin.Context) {
	c.HTML(http.StatusOK, "index", gin.H{
		"title": "首页",
		"site":  cfg.Get().Site,
	})
}

func (*viewApi) Install(c *gin.Context) {
	// 安装过
	if cfg.Get().Installed {
		resp.ErrorExit(c, gerr.TryInstall())
		return
	}
	c.HTML(http.StatusOK, "install", gin.H{
		"title":          "安装",
		"site":           cfg.Get().Site,
		"navigationHide": true,
	})
}
