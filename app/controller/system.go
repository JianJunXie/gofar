package controller

import (
	"gofar/app/model"
	"gofar/app/service"
	"gofar/packed/cfg"
	"gofar/packed/gerr"
	"gofar/packed/resp"

	"github.com/gin-gonic/gin"
)

var System = &systemApi{}

type systemApi struct{}

func (*systemApi) Install(c *gin.Context) {
	if cfg.Get().Installed {
		resp.ErrorExit(c, gerr.TryInstall())
		return
	}
	p := new(model.ParamInstall)
	if err := c.BindJSON(p); err != nil {
		resp.ErrorExit(c, gerr.Wrap(&err))
		return
	}
	if err := service.System.Install(p); err != nil {
		resp.ErrorExit(c, err)
		return
	}
	resp.SuccessData[*int](c, nil)
}
