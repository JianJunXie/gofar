package service

import (
	"gofar/app/dao"
	"gofar/app/model"
	"gofar/packed/cfg"
	"gofar/packed/gerr"
	"gofar/packed/orm"
	"gofar/packed/utils"
)

var System = &systemService{}

type systemService struct{}

func (*systemService) Install(conf *model.ParamInstall) *gerr.Error {
	// 更新数据库配置
	cfg.Get().Database = conf.Database
	// 初始化数据库并连接
	if err := orm.InitDB(); err != nil {
		return err
	}
	// 创建数据库表
	if err := dao.Table.CreateTable(); err != nil {
		return err
	}
	// 创建管理员用户
	res := dao.User.Create(&model.User{Account: conf.User.Account, Passwd: conf.User.Passwd, Name: "新用户_" + utils.RandString(6)})
	if res.Error != nil {
		return gerr.Swrap(&res.Error, "创建用户失败："+res.Error.Error())
	}
	// 安装状态
	cfg.Get().Installed = true
	// 更新配置文件
	if err := cfg.Marshal(); err != nil {
		cfg.Get().Installed = false
		return err
	}
	return nil
}
