package model

type ParamPage[T any] struct {
	Index     int      `form:"index" json:"index"`
	Size      int      `form:"size" json:"size"`
	Condition T        `form:"condition" json:"condition"`
	Asc       []string `form:"asc" json:"asc"`
	Desc      []string `desc:"desc" json:"desc"`
}

type ParamInstall struct {
	User struct {
		Account string
		Passwd  string
	}
	Database `json:"database"`
}
