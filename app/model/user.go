package model

type User struct {
	Account  string `gorm:"primaryKey"`
	Name     string
	Passwd   string
	Avatar   string
	Email    string
	CreateAt string
	UpdateAt string
}

func (User) TableName() string {
	return "gf_users"
}
