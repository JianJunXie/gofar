package model

type RespData[T any] struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data T      `json:"data"`
}

type PageInfo struct {
	Total int `json:"total"`
	Index int `json:"index"`
	Size  int `json:"size"`
}

type RespList[T any] struct {
	Code int32     `json:"code"`
	Msg  string    `json:"msg"`
	List T         `json:"list"`
	Page *PageInfo `json:"page"`
}
