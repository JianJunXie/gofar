package model

type Database struct {
	Type   string `json:"type"`
	Host   string `json:"host"`
	User   string `json:"user"`
	Passwd string `json:"passwd"`
	Port   int    `json:"port"`
	Dbname string `json:"dbname"`
}
type Server struct {
	Address     string `json:"address"`
	ServerRoot  string `json:"serverRoot"`
	ServerAgent string `json:"serverAgent"`
}
type Viewer struct {
}
type Site struct {
	Title       string       `json:"title"` // 标题
	Description string       `json:"description"`
	Logo        string       `json:"logo"`
	Icon        string       `json:"icon"`
	Navigation  []navigation `json:"navigation"`
}
type navigation struct {
	Name string `json:"name"`
	Path string `json:"path"`
}

type Config struct {
	Server    `json:"server"`
	Database  `json:"database"`
	Site      `json:"site"`
	Viewer    `json:"viewer"`
	Installed bool `json:"installed"` // 是否已安装
}
