package boot

import (
	"gofar/packed/cfg"
	"gofar/packed/orm"
)

func init() {
	// 读取配置
	cfg.Read()
	// 如果已经安装过则直接初始化SQL
	if cfg.Get().Installed {
		orm.InitDB()
	}
}
