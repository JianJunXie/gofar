package main

import (
	_ "gofar/boot"
	"gofar/packed/serve"
)

func main() {
	serve.Run()
}
