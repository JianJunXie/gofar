package resp

import (
	"gofar/app/model"
	"gofar/packed/gerr"
	"net/http"

	"github.com/gin-gonic/gin"
)

// 错误并退出
func ErrorExit(c *gin.Context, err *gerr.Error) {
	c.JSON(http.StatusBadRequest, model.RespData[any]{Code: err.Code, Msg: err.Text})
	c.Abort()
}

// 成功返回数据
func SuccessData[T any](c *gin.Context, data T) {
	c.JSON(http.StatusOK, model.RespData[T]{Code: 200, Msg: "ok", Data: data})
}

// 成功返回列表数据
func SuccessList[T *[]any](c *gin.Context, list T, page *model.PageInfo) {
	c.JSON(http.StatusOK, model.RespList[T]{Code: 200, Msg: "ok", List: list, Page: page})
}
