package gerr

type Error struct {
	Code int
	Text string
	err  *error
}

func (e *Error) GetErr() *error {
	if e.err != nil {

	}
	return e.err
}

func New(code int, text string, err *error) *Error {
	return &Error{Code: code, Text: text, err: err}
}

func NewCode(code int, text string) *Error {
	return &Error{Code: code, Text: text, err: nil}
}

func Wrap(err *error) *Error {
	return &Error{Code: 500, Text: "系统异常", err: err}
}

func Swrap(err *error, text string) *Error {
	return &Error{Code: 500, Text: text, err: err}
}
