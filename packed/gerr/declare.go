package gerr

// 预置错误
func NoPermission() *Error {
	return NewCode(1403, "权限不足")
}

// 预置错误
func TokenInvalid() *Error {
	return NewCode(1404, "Token失效")
}

// 预置错误
func UserOrPasswdFault() *Error {
	return NewCode(1405, "用户名密码错误")
}

// 预置错误
func SecurityFault() *Error {
	return NewCode(1406, "验证码错误")
}

// 预置错误
func UserEmpty() *Error {
	return NewCode(1407, "没有找到该用户")
}

// 预置错误
func PostEmpty() *Error {
	return NewCode(1501, "没有该文章")
}

// 预制错误
func UnknownDBtype() *Error {
	return NewCode(1601, "不支持的数据库类型")
}

// 预制错误
func TryInstall() *Error {
	return NewCode(1602, "已执行过安装")
}
